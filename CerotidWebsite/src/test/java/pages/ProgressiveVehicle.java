package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProgressiveVehicle {

	WebDriver driver = null;
	WebElement element = null;

	By clickVehicleYear = By.xpath("//*[@id=\"VehiclesNew_embedded_questions_list_Year\"]/ul/li[4]");
	By clickVehicleMake = By.xpath("// *[@id=\"VehiclesNew_embedded_questions_list_Make\"]/ul/li[5]");
	By clickVehicleModel = By.xpath("//*[@id=\"VehiclesNew_embedded_questions_list_Model\"]/ul/li[1]");
	By selectPrimaryUse = By.xpath("//select[@name='VehiclesNew_embedded_questions_list_VehicleUse']");
	By selectOwnOrLease = By.xpath("//select[@name='VehiclesNew_embedded_questions_list_OwnOrLease']");
	By selectHowLongHaveYouHadThisVehicle = By .xpath("//select[@name='VehiclesNew_embedded_questions_list_LengthOfOwnership']");
	By clickDone = By.xpath("//button[text()='Done']");
	By clickContinue = By.xpath("//button[text()='Continue']");

	public ProgressiveVehicle(WebDriver driver) {
		this.driver = driver;
	}

	public void clickVehicleYear(String VehicleYear) {
		driver.findElement(clickVehicleYear).click();
	}

	public void clickVehicleMake(String VehicleMake) {
		driver.findElement(clickVehicleMake).click();
	}

	public void clickVehiModel(String VehicleModel) {
		driver.findElement(clickVehicleModel).click();
	}

	public void selectPrimaryUse(String PrimaryUse) {
		element = driver.findElement(selectPrimaryUse);
		Select choosePrimaryUse = new Select(element);
		choosePrimaryUse.selectByIndex(2);
	}

	public void selectOwnOrLease(String OwnOrLease) {
		element = driver.findElement(selectOwnOrLease);
		Select chooseOwnOrLease = new Select(element);
		chooseOwnOrLease.selectByIndex(2);
	}

	public void selectHowLongHaveYouHadThisVehicle(String Howlonghaveyouhadthisvehicle) throws InterruptedException {

		element = driver.findElement(selectHowLongHaveYouHadThisVehicle);
		Select chooseHowLongHaveYouHadThisVehicle = new Select(element);
		Thread.sleep(1100);
		chooseHowLongHaveYouHadThisVehicle.selectByIndex(1);
	}

	public void clickDone() {
		driver.findElement(clickDone).click();
	}

	public void clickContinue() {
		driver.findElement(clickContinue).sendKeys(Keys.ENTER);
	}

}
