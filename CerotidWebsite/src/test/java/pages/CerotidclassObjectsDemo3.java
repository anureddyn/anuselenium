package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidclassObjectsDemo3 {
 
	WebDriver driver = null;
	WebElement element = null;
	
	
    By selectCourse = By.xpath("//select[@id = 'classType']");
	By SessionDate  = By.xpath("//select[@id='sessionType']");
	By enterFullName = By.xpath("//input[@id='name']");
	By enterAddress = By.xpath("//input[@id='address']");
	By enterCity = By.xpath("//input[@id='city']");
	By selectState = By.xpath("//select[@id='state']");
	By enterZipCode = By.xpath("//input[@id='zip']");
	By enterEmailId = By.xpath("//input[@id='email']");
	By enterPhoneNumber = By.xpath("//input[@id='phone']");
	By selectVisaStatus = By.xpath("//select[@id='visaStatus']");
	By selectHowDidYouHearAboutUs = By.xpath("//select[@id='mediaSource']");
	By clickAbleToRelocate = By.xpath("//div[@class='form-group']");
	By enterEducationalBackGround = By.xpath("//textarea[@id='eduDetails']");
	
	public CerotidclassObjectsDemo3(WebDriver driver) {
		this.driver = driver;
	}
	
	public void selectCourse(String courseName) {
		element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(courseName);
	}
	
	public void selectSessionDate(String sessionDate) {
		element = driver.findElement(SessionDate);
		Select chooseSession = new Select(element);
		chooseSession.selectByVisibleText(sessionDate);
	}
	public void enterFullName(String FullName) {
		driver.findElement(enterFullName).sendKeys(FullName);
	}
	public void enterAddress(String Address) {
		 driver.findElement(enterAddress).sendKeys(Address);
		
	}
	public void enterCity(String City) {
		driver.findElement(enterCity).sendKeys(City);
	}
	public void selectState(String State) {
		element = driver.findElement(selectState);
		Select chooseState = new Select(element);
		chooseState.selectByVisibleText(State);
	}
	public void enterZipCode(String ZipCode) {
		driver.findElement(enterZipCode).sendKeys(ZipCode);
	}
	public void enterEmailId(String EmailId) {
		driver.findElement(enterEmailId).sendKeys(EmailId);
	}
	public void enterPhoneNumber(String PhoneNumber) {
		driver.findElement(enterPhoneNumber).sendKeys(PhoneNumber);
		}
	public void selectVisaStatus(String VisaStatus) {
		element = driver.findElement(selectVisaStatus);
		Select chooseVisaStatus = new Select(element);
		chooseVisaStatus.selectByVisibleText(VisaStatus);
	}
	public void selectHowDidYouHearAboutUs(String HowDidYouHearAboutUs) {
		element = driver.findElement(selectHowDidYouHearAboutUs);
		Select chooseHowDidYouHearAboutUs = new Select(element);
		chooseHowDidYouHearAboutUs.selectByVisibleText(HowDidYouHearAboutUs);
	}
	public void clickAbleToRelocate(String AbleToRelocate) {
		driver.findElement(clickAbleToRelocate).click();
		}
	
	
	public void enterEducationalBackGround(String EducationalBackGround) {
		driver.findElement(enterEducationalBackGround).sendKeys(EducationalBackGround);
		}
	
}
