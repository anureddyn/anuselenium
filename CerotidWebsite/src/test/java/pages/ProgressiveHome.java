package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProgressiveHome {
	WebDriver driver = null;
	// WebElement element = null;
	private static WebElement element;

	By clickAuto = By.xpath("//p[@class='txt']");

	public ProgressiveHome(WebDriver driver) {
		this.driver = driver;
	}
	// course element

	public static WebElement clickAuto(WebDriver driver) {
		element = driver.findElement(By.xpath("//p[@class='txt']"));
		return element;
	}

	// ProgressiveHome.clickAuto(driver).click();
	public void clickAuto(String Auto) {
		driver.findElement(clickAuto).click();
		}
	
}
