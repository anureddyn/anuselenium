package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Google {
	
	WebDriver driver = null;
	//private static WebElement element;
	
	By SearchBox                   = By.xpath("//input[@id ='fakebox-input']");
	By enterSearchGoogle           = By.xpath("//input[@name ='q']");
	By clickSearchGoogle           = By.xpath("//input[@value ='Google Search']");
	
	
	public  Google(WebDriver driver) {
		this.driver = driver;
	}
	
	public void SearchBox(String Box) {
  		driver.findElement(SearchBox).sendKeys(Box); 
  	}
	public void enterSearchGoogle(String SearchGoogle) {
  		driver.findElement(enterSearchGoogle).sendKeys(SearchGoogle); 
  	}
	
	public void clickSearchGoogle() {
 	   driver.findElement(clickSearchGoogle).click();
    }

}
