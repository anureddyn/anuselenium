package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CerotidPage {
	private static WebElement element;

	// Course Element
	public static WebElement selectCourse(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id = 'classType']"));

		return element;

	}

	public static WebElement selectSessionDate(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;
	}

	public static WebElement enterFullName(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='name']"));
		return element;
	}

	public static WebElement enterAddress(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='address']"));

		return element;
	}

	public static WebElement enterCity(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='city']"));
		return element;
	}

	public static WebElement selectState(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='state']"));
		return element;
	}

	public static WebElement enterZipCode(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='zip']"));
		return element;
	}

	public static WebElement enterEmailId(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='email']"));
		return element;
	}

	public static WebElement enterPhoneNumber(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='phone']"));
		return element;
	}

	public static WebElement selectVisaStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		return element;
	}

	public static WebElement selectHowDidYouHearAboutUs(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		return element;
	}

	public static WebElement clickAbleToRelocate(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='form-group']"));
		return element;
	}

	public static WebElement enterEducationalBackGround(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;
	}
}
