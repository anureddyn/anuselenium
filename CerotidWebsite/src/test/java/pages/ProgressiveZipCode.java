package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;

public class ProgressiveZipCode {
	
  WebDriver driver = null;
	
   WebElement element = null;
	
	By enterZipCode = By.xpath("//input[@id='zipCode_overlay']");
	By clickToGetAQuote = By.xpath("//input[@id='qsButton_overlay']");
	
	public ProgressiveZipCode(WebDriver driver) {
		this.driver = driver;
	}
	
	//course element
	
	/*public static WebElement enterZipCode(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id=zipCode_overlay']"));
		return element;
		}*/
	
	public void enterZipCode(String ZipCode) {
		driver.findElement(enterZipCode).sendKeys(ZipCode); 
	}
	
	public void clickToGetAQuote(String GetAQuote) {
		driver.findElement(clickToGetAQuote).click();
	}
}
