package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class ProgressiveNameandAddress {

	  WebDriver driver = null;
	// WebElement element = null;

	  By enterFirstName    = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']");
	  By enterMiddleName   = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_MiddleInitial']");
	  By enterLastName     = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']");
      By enterBirthDate    = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']");
      By enterStreetNumberAndName = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_MailingAddress']");
      By enterAptNumber    = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']");
      By clickQuote        = By.xpath("//button[contains(text(),'Okay, start my quote.')]");
      
      
      
      public ProgressiveNameandAddress(WebDriver driver){
    	  this.driver = driver;
      }
      
      public void enterFirstName(String FirstName) {
  		driver.findElement(enterFirstName).sendKeys(FirstName); 
  	}
      public void enterMiddleName(String MiddleName) {
    		driver.findElement(enterMiddleName).sendKeys(MiddleName); 
    	}
      public void enterLastName(String LastName) {
  		driver.findElement(enterLastName).sendKeys(LastName); 
  	}
      public void enterBirthDate(String BirthDate) {
    		driver.findElement(enterBirthDate).sendKeys(BirthDate); 
    		driver.findElement(enterBirthDate).sendKeys(Keys.TAB); 
    	}
      
      public void enterStreetNumberAndName(String StreetNumber) throws InterruptedException {
    	  Thread.sleep(1100);
  		driver.findElement(enterStreetNumberAndName).sendKeys(StreetNumber); 
  	}
      public void enterAptNumber(String AptNumber) {
    		driver.findElement(enterAptNumber).sendKeys(AptNumber); 
    	}
       public void clickQuote() {
    	   driver.findElement(clickQuote).click();
       }


}


