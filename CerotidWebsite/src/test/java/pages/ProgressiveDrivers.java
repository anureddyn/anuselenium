package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProgressiveDrivers {

	static WebDriver driver = null;
	WebElement element = null;

	// By chooseGender
	// =By.xpath("//label[@id='DriversAddPniDetails_embedded_questions_list_Gender_Label']");
	By clickFemaleGender = By
			.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_F']");

	By clcikMaleGender = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']");

	By selectHighestLevelOfEducation = By
			.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']");

	By selectEmploymentStatus = By
			.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']");

	By selectMaritalStatus = By.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_MaritalStatus']");

	By selectPrimaryResidence = By
			.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']");

	By selectMovedInTheLastTwoMonths = By
			.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']");

	By selectLicenseStatus = By.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_LicenseStatus']");

	By selectYearsLicensed = By
			.xpath("//select[@name='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']");
	By clickClaims = By.xpath(
			"//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']");

	By clickTickets = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']");

	By clickContinue = By.xpath("//button[text() ='Continue']");

	public ProgressiveDrivers(WebDriver driver) {
		ProgressiveDrivers.driver = driver;
	}

	/*
	 * public void chooseGender(String value) { if(value.equalsIgnoreCase("Yes")) {
	 * driver.findElement(By.xpath(
	 * "//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_F']")).
	 * click(); }
	 * 
	 * else if(value.equalsIgnoreCase("No")) { driver.findElement(By.xpath(
	 * "//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']")).
	 * click(); }
	 */

	public void clickFemaleGender() throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(clickFemaleGender).click();

	}

	public void selectMaritalStatus(String MaritalStatus) {
		element = driver.findElement(selectMaritalStatus);
		Select chooseMaritalStatus = new Select(element);
		chooseMaritalStatus.selectByIndex(1);

	}

	public void selectHighestLevelOfEducation(String HighestLevelOfEducation) {
		element = driver.findElement(selectHighestLevelOfEducation);
		Select chooseHighestLevelOfEducation = new Select(element);
		chooseHighestLevelOfEducation.selectByIndex(7);

	}

	public void selectEmploymentStatus(String EmploymentStatus) {
		element = driver.findElement(selectEmploymentStatus);
		Select chooseEmploymentStatus = new Select(element);
		chooseEmploymentStatus.selectByIndex(5);

	}

	public void selectPrimaryResidence(String PrimaryResidence) {
		element = driver.findElement(selectPrimaryResidence);
		Select choosePrimaryResidence = new Select(element);
		choosePrimaryResidence.selectByIndex(1);

	}

	public void selectMovedInTheLastTwoMonths(String MovedInTheLastTwoMonths) {
		element = driver.findElement(selectMovedInTheLastTwoMonths);
		Select chooseMovedInTheLastTwoMonths = new Select(element);
		chooseMovedInTheLastTwoMonths.selectByIndex(1);

	}

	public void selectLicenseStatus(String LicenseStatus) {
		element = driver.findElement(selectLicenseStatus);
		Select chooseLicenseStatus = new Select(element);
		chooseLicenseStatus.selectByIndex(1);

	}

	public void selectYearsLicensed(String YearsLicensed) {
		element = driver.findElement(selectYearsLicensed);
		Select chooseYearsLicensed = new Select(element);
		chooseYearsLicensed.selectByIndex(1);

	}

	public void clickClaims(String Value) {
		driver.findElement(clickClaims).click();
	

	}

	public void clickTickets(String Value) throws InterruptedException {
		Thread.sleep(2000);
		driver.findElement(clickTickets).click();
	}

	public void clickContinue() {
		driver.findElement(clickContinue).sendKeys(Keys.ENTER);
	}

}
