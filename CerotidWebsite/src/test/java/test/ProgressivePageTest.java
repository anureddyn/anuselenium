package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.ProgressiveDrivers;
import pages.ProgressiveHome;
import pages.ProgressiveNameandAddress;
import pages.ProgressiveVehicle;
import pages.ProgressiveZipCode;

public class ProgressivePageTest {
	private static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {

		invokeBrowser();

		SignUpflow();

		closeBrowser();
	}

	private static void invokeBrowser() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("https://www.progressive.com");
		driver.manage().window().maximize();
		// Wait for the browser to load
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	private static void SignUpflow() throws InterruptedException {
		// TODO Auto-generated method stub
		// creating a page object
		ProgressiveHome obj = new ProgressiveHome(driver);
		obj.clickAuto("Yes");

		ProgressiveZipCode obj2 = new ProgressiveZipCode(driver);
		obj2.enterZipCode("75039");
		obj2.clickToGetAQuote("Yes");

		ProgressiveNameandAddress obj3 = new ProgressiveNameandAddress(driver);
		obj3.enterFirstName("Anuteja");
		obj3.enterMiddleName("Reddy");
		obj3.enterLastName("Neravetla");
		obj3.enterBirthDate("10/30/1989");
		obj3.enterStreetNumberAndName("5254 N O CONNOR Blvd");
		obj3.enterAptNumber("2501");
		obj3.clickQuote();

		Thread.sleep(5000);

		ProgressiveVehicle obj4 = new ProgressiveVehicle(driver);
		obj4.clickVehicleYear("2019");
		obj4.clickVehicleMake("Buick");
		obj4.clickVehiModel("Cascada");
		Thread.sleep(2000);
		obj4.selectPrimaryUse("2");
		Thread.sleep(2000);

		obj4.selectOwnOrLease("2");
		Thread.sleep(2000);
;
		obj4.selectHowLongHaveYouHadThisVehicle("1");
		obj4.clickDone();
		obj4.clickContinue();

		ProgressiveDrivers obj5 = new ProgressiveDrivers(driver);

	Thread.sleep(1100);

		// obj5.chooseGender("Yes");
		// obj5.chooseGender("No");
		obj5.clickFemaleGender();
		obj5.selectMaritalStatus("1");
		obj5.selectHighestLevelOfEducation("7");
		//Thread.sleep(1100);
		obj5.selectEmploymentStatus("5");
		Thread.sleep(1100);
		obj5.selectPrimaryResidence("1");
		//Thread.sleep(1100);
		obj5.selectMovedInTheLastTwoMonths("1");
		obj5.selectLicenseStatus("1");
		obj5.selectYearsLicensed("1");
	    Thread.sleep(2000);
		obj5.clickClaims("No");
		//Thread.sleep(1100);
		obj5.clickTickets("");
		//Thread.sleep(1100);
		obj5.clickContinue();

	}

	private static void closeBrowser() {
		// TODO Auto-generated method stub
		// driver.close();
		// driver.quit();

	}

}
