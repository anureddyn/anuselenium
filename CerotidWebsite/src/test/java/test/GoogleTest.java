package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.Google;

public class GoogleTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) {
		
		invokeBrowser();
		signflow();
		closeBrowser();
		
	}

	private static void invokeBrowser() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("https://www.google.com");
		driver.manage().window().maximize();
		// Wait for the browser to load
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
	}

	private static void signflow() {
		// TODO Auto-generated method stub
		Google obj6 = new Google(driver);
		obj6.SearchBox("");
		obj6.enterSearchGoogle("TestNG");
		obj6.clickSearchGoogle();
		
	}

	private static void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
		 //driver.quit();
		
	}

	
}
