package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidclassObjectsDemo3;

public class CerotidObjectsDemo3test {
 
	private static  WebDriver driver = null;
	
	public static void main(String[] args) {
		//step1 : Invoke Browser
		invokeBrowser();
		//Step 2: performing actions on the web page 
		cerotidSignUpFlow();
		//step 3: Close the Browser
		closeBrowser();
		
	}
	
	private static void invokeBrowser() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("http://www.cerotid.com");
		// Maximizing the Browser
				driver.manage().window().maximize();
				// Wait for the browser to load
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
	}
	
	private static void cerotidSignUpFlow() {
		// TODO Auto-generated method stub
		//creating a page object
		CerotidclassObjectsDemo3 obj = new CerotidclassObjectsDemo3(driver);
		obj.selectCourse("QA Automation");
		obj.selectSessionDate("Upcoming Session");
		obj.enterFullName("Anuteja R Neravetla");
		obj.enterAddress("5254 N O CONNOR Blvd");
		obj.enterCity("Irving");
		obj.selectState("TX");
		obj.enterZipCode("75039");
		obj.enterEmailId("anuneravetla3010@gmail.com");
		obj.enterPhoneNumber("5315007313");
		obj.selectVisaStatus("Other");
		obj.selectHowDidYouHearAboutUs("Friends/Family");
		obj.clickAbleToRelocate("Yes");
		obj.enterEducationalBackGround("Hi! I did my undergrad in Electronics & Communications, masters in Information Technology and Currently doing my Ph.D. in Information Technology.");
		
		
	}
	private static void closeBrowser() {
		// TODO Auto-generated method stub
		//driver.close();
		//driver.quit();
		
	}
	
	
}
