package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidPage;

public class CerotidPageTest {

	public static WebDriver driver = null;

	public static void main(String[] args) {

		invokeBrowser();
		fillForm();
	}

	public static void invokeBrowser() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// creating a new driver obj
		driver = new ChromeDriver();
		// Navigating to webpage
		driver.navigate().to("http://www.cerotid.com");

		// Maximizing the Browser
		driver.manage().window().maximize();
		// Wait for the browser to load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Form of Validation -- we are checking to see if the browser is getting
		// invoked or not
		String title = driver.getTitle();
		if (title.contains("Cerotid")) {
			System.out.println(driver.getTitle() + "-------------was launched");

		} else {
			System.out.println("Fail Browser was not invoked----------");
			System.exit(0);
		}

	}

	public static void fillForm() {
		// TODO Auto-generated method stub
		// Utilized the cerotidpage objects to select course.
		
		Select chooseCourse = new Select(CerotidPage.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");

		Select chooseSessionDate = new Select(CerotidPage.selectSessionDate(driver));
		chooseSessionDate.selectByVisibleText("Upcoming Session");
		CerotidPage.enterFullName(driver).sendKeys("Anuteja R Neravetla");
		CerotidPage.enterAddress(driver).sendKeys("5254 N O CONNOR Blvd #2501");
		CerotidPage.enterCity(driver).sendKeys("Irving");
		Select chooseState = new Select(CerotidPage.selectState(driver));
		chooseState.selectByVisibleText("TX");
		CerotidPage.enterZipCode(driver).sendKeys("75039");
		CerotidPage.enterEmailId(driver).sendKeys("anuneravetla3010@gmail.com");
		CerotidPage.enterPhoneNumber(driver).sendKeys("5315007313");
		Select chooseVisaStatus = new Select(CerotidPage.selectVisaStatus(driver));
		chooseVisaStatus.selectByVisibleText("Other");
		Select chooseHowDidYouHearAboutUs = new Select(CerotidPage.selectHowDidYouHearAboutUs(driver));
		chooseHowDidYouHearAboutUs.selectByVisibleText("Friends/Family");
		CerotidPage.clickAbleToRelocate(driver).click();
		// chooseAbleToRelocate.clickByVisibleText("No");
		CerotidPage.enterEducationalBackGround(driver).sendKeys(
				"Hi! I did my undergrad in Electronics & Communications, masters in Information Technology and Currently doing my Ph.D. in Information Technology.");
	}
}