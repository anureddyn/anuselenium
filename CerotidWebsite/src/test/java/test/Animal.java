package test;

//Base Class
public class Animal {
 
	
	private String sex;
	private int numOfLegs;
	private boolean hasTail;
	private String eat;
	private String sleep;
	private String makeNoise;
	private boolean isDomestic;
	
	//Default Constructor
	public Animal() {
		}

	public Animal(String sex, int numOfLegs, boolean hasTail, String eat, String sleep, String makeNoise,
			boolean isDomestic) {
		super();
		this.sex = sex;
		this.numOfLegs = numOfLegs;
		this.hasTail = hasTail;
		this.eat = eat;
		this.sleep = sleep;
		this.makeNoise = makeNoise;
		this.isDomestic = isDomestic;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getNumOfLegs() {
		return numOfLegs;
	}

	public void setNumOfLegs(int numOfLegs) {
		this.numOfLegs = numOfLegs;
	}

	public boolean isHasTail() {
		return hasTail;
	}

	public void setHasTail(boolean hasTail) {
		this.hasTail = hasTail;
	}

	public String getEat() {
		return eat;
	}

	public void setEat(String eat) {
		this.eat = eat;
	}

	public String getSleep() {
		return sleep;
	}

	public void setSleep(String sleep) {
		this.sleep = sleep;
	}

	public String getMakeNoise() {
		return makeNoise;
	}

	public void setMakeNoise(String makeNoise) {
		System.out.println("Make a generic noise");
		this.makeNoise = makeNoise;
	}

	public boolean isDomestic() {
		return isDomestic;
	}

	public void setDomestic(boolean isDomestic) {
		this.isDomestic = isDomestic;
	}

	@Override
	public String toString() {
		return "Animal [sex=" + sex + ", numOfLegs=" + numOfLegs + ", hasTail=" + hasTail + ", eat=" + eat + ", sleep="
				+ sleep + ", makeNoise=" + makeNoise + ", isDomestic=" + isDomestic + "]";
	}
	
	
}

