package TestNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SimpleTestingClassDemo {
                 
	 static WebDriver driver;
	 
	 @BeforeTest
	 public void setUpTest() {
		 System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
	 		driver = new ChromeDriver();
	 }
	
	/*public static void main(String[] args) {
		
		//step 1 : invoke the Browser
		navigateToGoogle();
		
		//step 2 : Perform actions
		performActions();
		
		
		//step 3: Terminate test
		terminateTest();
	}*/

	
     private static void navigateToGoogle() {
		// TODO Auto-generated method stub
    	 
 		driver.navigate().to("https://www.google.com");
		driver.manage().window().maximize();
		// Wait for the browser to load
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
	}
	@Test
	private static void performActions() {
		// TODO Auto-generated method stub
		navigateToGoogle();
		
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		WebElement searchBox = driver.findElement(By.xpath("//input[@name='btnK']"));
		
		txtBoxSearch.sendKeys("What is testing");
		searchBox.sendKeys(Keys.RETURN);
	}
	@AfterTest
	public static void terminateTest() {
		// TODO Auto-generated method stub
		
		driver.close();
		driver.quit();
	}


	
}
