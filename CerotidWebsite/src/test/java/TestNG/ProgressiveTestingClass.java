package TestNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.ProgressiveHome;
import pages.ProgressiveNameandAddress;
import pages.ProgressiveZipCode;

public class ProgressiveTestingClass {

	static WebDriver driver; // = null;
	
	
	@BeforeSuite
	public void executeBeforeStartingTest() {
		//Add extent objs
	}

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	/*
	 * public static void main(String[] args) {
	 * 
	 * //step 1: invoke the browser invokeBrowser(); //step 2: perform actions
	 * SignUpflow(); //step 3: terminating the browser terminatingBrowser(); }
	 */

	private static void navigateToProgressive() {
		// TODO Auto-generated method stub

		driver.navigate().to("https://www.progressive.com");
		driver.manage().window().maximize();
		// Wait for the browser to load
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

   @Test
	private static void homepage() {
		// TODO Auto-generated method stub
		navigateToProgressive();

		// creating a page object
		ProgressiveHome obj = new ProgressiveHome(driver);
		obj.clickAuto("Yes");
   }
	@Test 
	private static void homepagecontinue() {
		ProgressiveZipCode obj2 = new ProgressiveZipCode(driver);
		obj2.enterZipCode("75039");
		obj2.clickToGetAQuote("Yes");
	}
      @Test 
      private static void hompageredirectstoperosndetails() throws InterruptedException {
		ProgressiveNameandAddress obj3 = new ProgressiveNameandAddress(driver);
		obj3.enterFirstName("Anuteja");
		obj3.enterMiddleName("Reddy");
		obj3.enterLastName("Neravetla");
		obj3.enterBirthDate("10/30/1989");
	    obj3.enterStreetNumberAndName("5254 N O CONNOR Blvd");
		obj3.enterAptNumber("2501");
		obj3.clickQuote();
      }

   

	@AfterTest
	private static void terminatingBrowser() {
		// TODO Auto-generated method stub
		driver.close();
		driver.quit();
      System.out.println("Terminating the Browser");
	
	}
   @AfterSuite
   public void tearDown() {
	   
   }
   
}
