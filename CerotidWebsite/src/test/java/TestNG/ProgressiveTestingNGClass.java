package TestNG;

//import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;

//import pages.Google;
import pages.ProgressiveDrivers;
import pages.ProgressiveHome;
import pages.ProgressiveNameandAddress;
import pages.ProgressiveVehicle;
import pages.ProgressiveZipCode;

public class ProgressiveTestingNGClass {
	

	static WebDriver  driver;
	@BeforeSuite
	public void executeBeforeStartingTest() {
		//Add extent objs
	}

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

		private static void navigateToProgressive() {
		// TODO Auto-generated method stub

		driver.navigate().to("https://www.progressive.com");
		driver.manage().window().maximize();
		// Wait for the browser to load
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

   @Test(priority = 0)
	private static void Anutejahomepage() {
		// TODO Auto-generated method stub
		navigateToProgressive();

		// creating a page object
		ProgressiveHome obj = new ProgressiveHome(driver);
		obj.clickAuto("Yes");
   }
	@Test(priority = 1)
	private static void homepagecontinue() {
		ProgressiveZipCode obj2 = new ProgressiveZipCode(driver);
		obj2.enterZipCode("75039");
		obj2.clickToGetAQuote("Yes");
	}
      @Test(priority=2)
      private static void hompageredirectstoperosndetails() throws InterruptedException {
		ProgressiveNameandAddress obj3 = new ProgressiveNameandAddress(driver);
		obj3.enterFirstName("Anuteja");
		obj3.enterMiddleName("Reddy");
		obj3.enterLastName("Neravetla");
		obj3.enterBirthDate("10/30/1989");
	    obj3.enterStreetNumberAndName("5254 N O CONNOR Blvd");
		obj3.enterAptNumber("2501");
		obj3.clickQuote();
      }
      
      //Thread.sleep(6000);
       @Test(priority=3)
        private static void ProgressiveVehicleDetails() throws InterruptedException {
		ProgressiveVehicle obj4 = new ProgressiveVehicle(driver);
		obj4.clickVehicleYear("2019");
		obj4.clickVehicleMake("Buick");
		obj4.clickVehiModel("Cascada");
		Thread.sleep(2000);
		obj4.selectPrimaryUse("2");
		Thread.sleep(2000);

		obj4.selectOwnOrLease("2");
		Thread.sleep(2000);
		obj4.selectHowLongHaveYouHadThisVehicle("1");
		obj4.clickDone();
		obj4.clickContinue();
}
      @Test(priority=4)
       private static void ProgressiveDriversDetails() throws InterruptedException {
		
    	  ProgressiveDrivers obj5 = new ProgressiveDrivers(driver);

		Thread.sleep(5000);

		// obj5.chooseGender("Yes");
		// obj5.chooseGender("No");
		obj5.clickFemaleGender();
		obj5.selectMaritalStatus("1");
		obj5.selectHighestLevelOfEducation("7");
		Thread.sleep(1100);
		obj5.selectEmploymentStatus("5");
		obj5.selectPrimaryResidence("1");
		Thread.sleep(1100);
		obj5.selectMovedInTheLastTwoMonths("1");
		obj5.selectLicenseStatus("1");
		obj5.selectYearsLicensed("1");
	    Thread.sleep(1100);
		obj5.clickClaims("No");
		//Thread.sleep(2000);
		obj5.clickTickets("No");
		//Thread.sleep(1100);
		obj5.clickContinue();

	}
      
         @AfterTest
    
   private static void terminatingBrowser() throws InterruptedException {
		// TODO Auto-generated method stub
    	Thread.sleep(1000);
		//driver.close();
		//driver.quit();
      System.out.println("Terminating the Browser");
	
	}
   //@AfterMethod
   
   @AfterSuite
   public void tearDown() {
	   
   }
   
}

