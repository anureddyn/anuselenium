package TestNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportSimpleClassDemoTest {
	
	// Global Variables
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;
	WebDriver driver;
	
	@BeforeSuite
	public void setUp() {
		
		//creating ExtentReports and attach the report
		htmlReporter = new ExtentHtmlReporter("cerotidWebsite.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}
	
	@BeforeTest
	 public void setUpTestInvokeBrowser() {
		//Setting System path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		//creating the driver obj
		driver = new ChromeDriver();
        ExtentTest test = null;
		test.log(Status.INFO, "Executing the test case");
		
	}
	public void navigateToWebPage() {
		driver.get("http://www.cerotid.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		@Test
        public void cerotidTestPage() {
			
			//Go tot cerotid page
			navigateToWebPage();
			extentTest test2
			if(driver.getTitle().contains("cerotid")) {
				test.pass("Navigated to cderotid Website as Expected");
			}else {
				test.fail("Did not navigate to cerotid website");
			}
		}
	
	
	}

}
